//project.c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "ppm_io.h"
#include "imageManip.h"

int main(int argc, char*argv[]) {
  if(argc<2) {//print error if there is less than two arguments
    printf("Failed to supply input filename or output filename or both.");
    return 1;
  }
  FILE *fp = fopen(argv[1],"rb"); //define file pointer for input file 
  if(fp==NULL) {
    printf("Specified input file could not be opened"); //prints error if file pointer is null (file could not be opened)
    return 2;
  }
  Image *im = read_ppm(fp); //read in values of input file into address pointed to by pointer im
  if(im==NULL) { //if pointer im is null, print error
    printf("Specified input file is not a properly formatted PPM file.");
    return 3;
  }
  if(!argv[3]) //if there is no third argument specifying what operation should be called 
  {
    printf("Please specify an operation name.");
    return 4;
  }
  if(strcmp(argv[3],"swap")==0) //if user calls swap as argument
     swap(im);
  else if(strcmp(argv[3],"grayscale")==0) //if user calls grayscale as argument
    grayscale(im);
  else if(strcmp(argv[3],"contrast")==0) { //if user calls contrast as argument
    printf("By what factor do you want to change contrast? ");//asks user what factor to change contrast by
    float factor;
    if(scanf("%f",&factor)==1)//scan user input into variable factor
       contrast(im,factor);
    else {
      printf("Incorrect number or kind of arguments for this operation");//if the user does not input a float as an argument
      return 5;
    }      
  }
  else if(strcmp(argv[3],"zoom_in")==0) {//if user calls zoom in as argument
    Image *temp = im;
    im = zoom_in(im);//set pointer im to point to new image data 
    free(temp->data);//free data that was allocated in original im 
    free(temp);
    temp = NULL;
  }
  else if(strcmp(argv[3],"zoom_out")==0) { //if user calls zoom out as argument
    Image *temp = im;
    im = zoom_out(im);//set pointer im to point to new image data
    free(temp->data);//free data that was allocated in original im
    free(temp);
    temp = NULL;
  }
  else if(strcmp(argv[3],"occlude")==0) {//if user calls occlude as argument
    printf("Enter coordinates x1,y1,x2,y2 in that order: ");//prompt user to give coordinates for box to occlude in image 
    int x1,x2,y1,y2;
    if(scanf("%d %d %d %d",&x1,&y1,&x2,&y2)==4) {//scan user input into variables
      if(x1>0 && x1<im->rows && y1>0 && y1<im->cols && x2>0 && x2<im->rows && y2>0 && y2<im->cols && x1<x2 && y1<y2)//checks if input values are valid for function 
	occlude(x1,y1,x2,y2,im);
      else {//print error if inputs are invalid for function
	printf("Arguments for occlude operation were out of range for the given input image");
	return 6;
      }
    }
    else {//if user does not input enough arguments or wrong kinds of inputs for function 
      printf("Incorrect number or kind of arguments for this operation");
      return 5;
    }
  }  
  else if(strcmp(argv[3],"blur")==0) {//if user calls blur as argument
    printf("Enter blur factor: ");//prompt user to give blur factor
    double d;
    if(scanf("%lf",&d)==1) {//scan user input into variable d
      Image *temp = im;
      im = blur(im,d);
      free(temp->data);//free data that was allocated in original im
      free(temp);
    }
    else {//if user does not input a double as an argument
      printf("Incorrect number or kind of arguments for this operation");
      return 5;
    }
  }
  else {//if user did not supply an argument that was one of the functions in this program 
    printf("Operation name specified was invalid");
    return 4;
  }
  fp = fopen(argv[2],"wb");//open output file for writing
  if(fp==NULL) {//if output file does not open
    printf("Specified output file could not be opened for writing");
    return 7;
  }
  int num = write_ppm(fp,im);//write data pointed to by im into file pointed to by fp
  if(num==0) {//if no pixels were written
    printf("Failed to write pixels to file. Number of pixels written is 0.");
    return 7;
  }
  fclose(fp);
  printf("Image created with %d pixels.\n", num);//prints number of pixels printed to output file to user
  free(im->data);//free memory associated with image data
  free(im);//free memory associated with pointer im

  return 0;
}
