// ppm_io.c
// 601.220, Fall 2018

#include <assert.h>
#include "ppm_io.h"
#include <stdio.h>
#include <stdlib.h>


/* Read a PPM-formatted image from a file (assumes fp != NULL).
 * Returns the address of the heap-allocated Image struct it
 * creates and populates with the Image data.
 */

Image * read_ppm(FILE *fp) {

  Image *img;
  char buff[16];
  int rgb_comp_color;
  int NeededRGB = 255;
  if(!fp) {//prints error if input file can not be opened   
    printf("Specified input file could not be opened");
    exit(2);
  }
  if(!fgets(buff, sizeof(buff), fp)) {//returns error if file cannot be read
    printf("Reading input failed");
    exit(3);
  }
  if(buff[0] != 'P' || buff[1] != '6') {//returns error if file is not properly formatted
    printf("Invalid image format (must be 'P6')");
    exit(3);
  }

  img = (Image*)malloc(sizeof(Image));//allocate memory to pointer img

  int c = getc(fp);
  while (c=='#') {//skips over first comment line
    while ((c = getc(fp)) != '\n') {
      }
  }
  ungetc(c,fp);//if there is no comment line, goes back one char
  if(fscanf(fp," %d %d", &img->cols, &img->rows) != 2){//scans in values for img dimensions
    printf("Invalid image size\n");//print error if invalid image size
    exit(8);
  }

  if(fscanf(fp,"%d",&rgb_comp_color)!=1){ 
    printf("Invalid rgb component\n");
    exit(8);
  }

  if(rgb_comp_color!=NeededRGB){
    printf("No 8-bit components in file\n");
    exit(8);
  }
  
  while(fgetc(fp)!='\n');
  img->data = (Pixel*)malloc(img->cols*img->rows*sizeof(Image));

  if(!img){//print error if img pointer is null 
    printf("Unable to allocate memory\n");
    exit(8);
  }

  if((int)fread(img->data,3*img->cols,img->rows,fp)!=img->rows){
    printf("Error loading image\n");
    exit(8);
  }
  fclose(fp);//close file when done reading
  return img; 
  
}


/* Write a PPM-formatted image to a file (assumes fp != NULL),
 * and return the number of pixels successfully written.
 */
int write_ppm(FILE *fp, const Image *im) {

  // check that fp is not NULL
  assert(fp); 

  // write PPM file header, in the following format
  // P6
  // cols rows
  // 255
  fprintf(fp, "P6\n%d %d\n255\n", im->cols, im->rows);

  // now write the pixel array
  int num_pixels_written = fwrite(im->data, sizeof(Pixel), im->rows * im->cols, fp);
  // print error if pixel data did not write properly
  if (num_pixels_written != im->rows * im->cols) {
    fprintf(stderr, "Uh oh. Pixel data failed to write properly!\n");
  }

  return num_pixels_written;
}

