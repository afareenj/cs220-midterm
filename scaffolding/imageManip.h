// imageManip.h
// 601.220, Fall 2018

#ifndef IMAGEMANIP_H
#define IMAGEMANIP_H
#define PI 3.14159265358979323846264338327950288
#include <stdio.h>
#include <math.h>
#include "ppm_io.h"
void swap(Image *im);
void grayscale(Image *im);
void contrast(Image *im, double input);
Image* zoom_in(Image *im);
Image* zoom_out(Image *im);
void occlude(int x1, int y1, int x2, int y2, Image *im);
Image* blur(Image *im, double sigma);
double* gaussian(double sigma);
#endif
