// imageManip.c

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "imageManip.h"
#include "ppm_io.h"
//function to swap image color channels
void swap(Image *im)
{
  unsigned char temp;
  for (int r = 0; r < im->rows; r++) { //iterates pixel by pixel
    for (int c = 0; c < im->cols; c++) {
      temp = (im->data[(r*im->cols)+c]).r; //temp holds original r value of pixel
      (im->data[(r*im->cols)+c]).r = (im->data[(r*im->cols)+c]).g; //r value of pixel swapped to g value
      (im->data[(r*im->cols)+c]).g = (im->data[(r*im->cols)+c]).b; //g value of pixel swapped to b value
      (im->data[(r*im->cols)+c]).b = temp; //b value of pixel swapped to temp (r value)
    }
  }
}
//function to turn image black and white
void grayscale(Image *im)
{
  unsigned char temp;
  for (int r = 0; r < im->rows; r++) { //iterates pixel by pixel
    for (int c = 0; c < im->cols; c++) {
      temp = (0.30*(im->data[(r*im->cols)+c]).r) + (0.59*(im->data[(r*im->cols)+c]).g) + (0.11*(im->data[(r*im->cols)+c]).b); //NTSC standard conversion formula stored in term
      (im->data[(r*im->cols)+c]).g = temp; //sets rgb values of pixel to NTSC formula value for pixel
      (im->data[(r*im->cols)+c]).b = temp;
      (im->data[(r*im->cols)+c]).r = temp;
      
    }
  }
 }
//function to increase or decrease contrast of image, with a double input of less than 1 decreasing contrast and greater than 1 increasing contrast 
void  contrast(Image *im, double input)
{
  double temp;
  for (int r = 0; r < im->rows; r++) { //iterates pixel by pixel
    for (int c = 0; c < im->cols; c++) {
      temp = (double)((im->data[(r*im->cols)+c]).r); //sets temp to r value of pixel
      temp = 255*((temp/255 - 0.5)*input+0.5); //sets temp value to within (-0.5,0.5), then multiplies this value by input and adds 0.5, and finally multiplies by 255 to get the contrast r value of pixel
      if(temp > 255)
	temp = 255; //anything above highest possible r value of 255 set to 255
      if(temp < 0)
	temp = 0; //anything below lowest possible r value of 0 set to 0
      (im->data[(r*im->cols)+c]).r = (unsigned char)((int) temp);
      temp = (double)((im->data[(r*im->cols)+c]).g); //temp is set to g value of pixel and process is repeated
      temp = 255*((temp/255 - 0.5)*input+0.5);
      if(temp > 255)
        temp = 255;
      if(temp < 0)
        temp = 0;
      (im->data[(r*im->cols)+c]).g = (unsigned char)((int) temp);
      temp = (double)((im->data[(r*im->cols)+c]).b); //temp is set to b value of pixel and process is repeated
      temp = 255*((temp/255 - 0.5)*input+0.5);
      if(temp > 255)
        temp = 255;
      if(temp < 0)
        temp = 0;
      (im->data[(r*im->cols)+c]).b = (unsigned char)((int) temp);
    } 
  }

}
//function to zoom into an image, doubling the number of pixel rows and columsn used to display the image
Image* zoom_in(Image *im)
{
  
  Image *image2 = malloc(sizeof(Image)*4*(im->rows)*(im->cols)); //dynamically allocating memory for second image, which will hold zoomed in image
  int newrows = im->rows*2; //need double the rows and columns for zoomed in image
  int newcols = im->cols*2;
  image2->rows = newrows;
  image2->cols = newcols;
  image2->data = (Pixel*)malloc(image2->cols*image2->rows*sizeof(Image)); //dynamically allocating memory for pixels of second image
  for (int r = 0; r < image2->rows; r++) { //iterates pixel by pixel
    for (int c = 0; c < image2->cols; c++) {
      int oldr = r/2;//index for old image row
      int oldc = c/2;//index for old image col
      image2->data[r*image2->cols+c] = im->data[oldr*im->cols+oldc]; //places original image pixel data at row x and column y into zoomed in pixel data at row 2x and colun 2y, row 2x+1 and column 2y, row 2x and column 2y+1, and row 2x+1 and column 2y+1
    }
  }
  return image2;
}
//function to zoom out from an image, halving the number of pixel rows and columns used to display the image
Image* zoom_out(Image *im)
{
  Image *image2;
  int newrows = im->rows/2; //need half the rows and columns for zoomed out image
  int newcols = im->cols/2;
  image2 = (Image*)malloc(sizeof(Image)*newrows*newcols); //dynamically allocating memory for second image, which will hold zoomed out image
  image2->rows = newrows;
  image2->cols = newcols;
  image2->data = (Pixel*)malloc(image2->cols*image2->rows*sizeof(Image)); //dynamically allocating memory for pixels of second image
  for (int r = 0; r < image2->rows; r++) { //iterate pixel by pixel
    for (int c = 0; c < image2->cols; c++) {
      (image2->data[(r*image2->cols)+c].g) = ((im->data[(2*r*im->cols)+2*c].g)+(im->data[(2*r*im->cols)+2*c+1].g)+(im->data[((2*r+1)*im->cols)+2*c].g)+(im->data[((2*r+1)*im->cols)+2*c+1].g))/4; //sets zoomed out image pixel g values at row x and column y to the average of original image pixel g values at row 2x and column 2y, row 2x+1 and column 2y, row 2x and column 2y+1, and row 2x+1 and column 2y+1
      (image2->data[(r*image2->cols)+c].b) = ((im->data[(2*r*im->cols)+2*c].b)+(im->data[(2*r*im->cols)+2*c+1].b)+(im->data[((2*r+1)*im->cols)+2*c].b)+(im->data[((2*r+1)*im->cols)+2*c+1].b))/4; //same but for b values
      (image2->data[(r*image2->cols)+c].r) = ((im->data[(2*r*im->cols)+2*c].r)+(im->data[(2*r*im->cols)+2*c+1].r)+(im->data[((2*r+1)*im->cols)+2*c].r)+(im->data[((2*r+1)*im->cols)+2*c+1].r))/4; //same but for r values
    }
  }
  return image2;
}
//function to black out a rectangle specified by the user with top left corner located at pixel row x1 and column y1 and bottom right corner located at pixel row x2 and column y2
void occlude(int x1, int y1, int x2, int y2, Image *im)
{
  for (int r = y1; r <= y2; r++) { //iterates through pixels located within specified rectangle
    for (int c = x1; c <= x2; c++) {
      (im->data[(r*im->cols)+c]).g = (unsigned char) 0; //sets rgb values of pixels in rectangle to 0, in order to make the pixel black
      (im->data[(r*im->cols)+c]).r = (unsigned char) 0;
      (im->data[(r*im->cols)+c]).b = (unsigned char) 0;
    }
  }
}
//function to blur image quality based on a user-inputted sigma value, with higher sigma values corresponding to a greater blur effect
Image * blur(Image *im, double sigma) {
  //define new image pointer
  Image *image2 = malloc(sizeof(Image)*(im->rows)*(im->cols)); //dynamically allocates memory for new image
  image2->rows = im->rows;
  image2->cols = im->cols;
  image2->data = (Pixel*)malloc(image2->cols*image2->rows*sizeof(Image)); //dynamically allocates memory for pixels of new image
  int gridrows = (10*sigma)+((int)(10*sigma-1)%2); //row length of gaussian matrix
  double *grid = malloc(sizeof(double)*gridrows*gridrows); //dynamically allocating memory for gaussian matrix 
  grid = gaussian(sigma); //creates gaussian matrix based on our gaussian function below
  int center = gridrows/2;
  double sum = 0;
  double sumr = 0;
  double sumg = 0;
  double sumb = 0;  
  int row = im->rows;
  int col = im->cols;
  for (int r = 0; r < row; r++) { //iterates through pixel rows
    for (int c = 0; c < col; c++) { //iterates through pixel columns
      for (int i = center-r; i < gridrows; i++) { //iterates through grid rows
	for (int j = center-c; j < gridrows; j++) { //iterates through grid columns
	  if (i>=0 && j>=0 && (c+j)<(col+center) && (r+i)<(row+center) ) { //if grid point is within the rows and columns of image
	    sumr = sumr + ((im->data[(r-center+i)*im->cols+(c-center+j)]).r * grid[i*gridrows+j]); //add gaussian matrix value times pixel r value at that point to cumulative r value sum
	    sumg = sumg + ((im->data[(r-center+i)*im->cols+(c-center+j)]).g * grid[i*gridrows+j]); //same but for g value
	    sumb = sumb + ((im->data[(r-center+i)*im->cols+(c-center+j)]).b * grid[i*gridrows+j]); //same but for b value
	    sum+=grid[i*gridrows+j]; //keep track of total sum of gaussian matrix values for normalization later
	  }
	}
      }
      sumr = sumr/sum; //normalization for rgb sum values
      sumg = sumg/sum;
      sumb = sumb/sum;
      (image2->data[r*image2->cols+c]).r = sumr; //set rgb values of new image to rgb sum values 
      (image2->data[r*image2->cols+c]).g = sumg;
      (image2->data[r*image2->cols+c]).b = sumb;
      sum = 0; //reset sum values for next pixel
      sumr = 0; 
      sumg = 0;
      sumb = 0;
    }
  }
  free(grid); //free memory 
  return image2;
}
//function which assists our blur function, generating a square matrix with row and column length depending on the user-inputted sigma value
double* gaussian(double sigma) {
  int size = (10*sigma)+((int)(10*sigma-1)%2); //calculates what number of rows and columns should be in matrix
  double *grid = malloc(sizeof(double)*size*size); //dynamically allocates memory for grid
  double dx;
  double dy;
  int center = size/2; //row and column value at center of grid
  double g;
  for (int i = 0; i<size; i++) { //iterates through grid
    for (int j = 0; j<size; j++) {
      dx = (double)abs(i-center); //row distance of grid point from center
      dy = (double)abs(j-center); //column distance of grid point from center
      g = (1.0 / (2.0 * PI * sigma*sigma)) * exp( -(dx*dx + dy*dy) / (2*sigma*sigma)); //formula to calculate gaussian value
      grid[i*size+j] = g; //set grid point to gaussian value
    }
  }
  return grid;
}
